/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package main

// Creates a Master Key

import (
	
	F	"path/filepath"
	M	"git.duniter.org/gerard94/util/misc"
	R	"git.duniter.org/gerard94/util/resources"
		"git.duniter.org/gerard94/util/alea"
		"fmt"
		"encoding/hex"
		"os"

)

const (
	
	keySize = 256 / 8
	
	keyPath = "skillsKeys/masterKey.txt"

)

var (
	
	keyP = R.FindDir(keyPath)

)

func main () {
	var key [keySize]byte
	for i := range key {
		key[i] = byte(alea.IntRand(0, 0x100))
	}
	f, err := os.Create(keyP); M.Assert(err == nil, err, 100)
	defer f.Close()
	fmt.Fprint(f, "\"", hex.EncodeToString(key[:]), "\"")
	fmt.Println("Master key created in", keyP)
}

func init () {
	d, err := F.Abs(F.Dir(keyP)); M.Assert(err == nil, err, 100)
	os.MkdirAll(d, 0o777)
}
