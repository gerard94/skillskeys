/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package cryptoBasic

import (
	
	J	"git.duniter.org/gerard94/util/json"
	M	"git.duniter.org/gerard94/util/misc"
	R	"git.duniter.org/gerard94/util/resources"
		"crypto/aes"
		"encoding/base64"
		"errors"
		"flag"
		"fmt"
		"encoding/hex"
		"crypto/hmac"
		"os"
		"strings"
		"text/scanner"
		"crypto/sha256"
		"time"

)

const (
	
	version = "1.0.5"
	
	keySize = 256 / 8
	keyPath = "skillsclient/managerKey.txt"
	
	tokenValidity = "1h"

)

var (
	
	keyP = R.FindDir(keyPath)
	manKey string
	key []byte
	
	tokenDuration time.Duration

)

func Version () string {
	return version
}

func getMKeys (key1, key2 *[]byte) bool {
	
	get1K := func (s *scanner.Scanner) []byte {
		s.Scan()
		ss := s.TokenText()
		M.Assert(ss[0] == '"' && ss[len(ss) - 1] == '"', ss, 101)
		k, err := hex.DecodeString(ss[1:len(ss) - 1]); M.Assert(err == nil, err, 100)
		return k
	}
	
	f, err := os.Open(keyP)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Incorrect path to Manager Key")
		return false
	}
	defer f.Close()
	s := new(scanner.Scanner)
	s.Init(f)
	s.Error = func(s *scanner.Scanner, msg string) {M.Halt(errors.New("File " + keyP + " incorrect"), 100)}
	s.Mode = scanner.ScanStrings
	k := get1K(s)
	M.Assert(len(k) == keySize, "key has wrong length:", k, 103)
	*key1 = k
	k = get1K(s)
	*key2 = k
	return true
}

func GetKey () bool {
	
	equal := func (k1, k2 []byte) bool {
		if len(k1) != len(k2) {
			return false
		}
		for i := range k1 {
			if k1[i] != k2[i] {
				return false
			}
		}
		return true
	}
	
	if manKey == "" {
		flag.PrintDefaults()
		return false
	}
	
	var mk, mkh []byte
	if !getMKeys(&mk, &mkh) {
		return false
	}
	
	h := sha256.New()
	h.Write([]byte(manKey))
	mh := h.Sum(nil)
	M.Assert(len(mh) == keySize, len(mh), 100)
	
	b, err := aes.NewCipher(mh); M.Assert(err == nil, err, 101)
	bSize := b.BlockSize()
	M.Assert(keySize % bSize == 0 && keySize / bSize > 0, "bSize =", bSize)
	key = make([]byte, keySize)
	for i := 0; i < keySize; i += bSize {
		b.Decrypt(key[i: i + bSize], mk[i: i + bSize])
	}
	
	h.Reset()
	h.Write([]byte(key))
	kh := h.Sum(nil)
	
	if !equal(kh, mkh) {
		fmt.Fprintln(os.Stderr, "Error: wrong key")
		return false
	}
	return true
}

func CryptPass (pass string) string {
	h := hmac.New(sha256.New, key)
	h.Write([]byte(pass))
	return base64.RawStdEncoding.EncodeToString(h.Sum(nil))
}

func VerifyPass (test, model string) bool {
	return CryptPass(test) == model
}

func MakeToken (nickname string, admin bool) string {
	mk := J.NewMaker()
	mk.StartObject()
	mk.PushString(nickname)
	mk.BuildField("nickname")
	mk.PushBoolean(admin)
	mk.BuildField("admin")
	mk.PushInteger(time.Now().Add(tokenDuration).Unix())
	mk.BuildField("exp")
	mk.BuildObject()
	payload := []byte(mk.GetJson().GetFlatString())
	encPayload := base64.RawURLEncoding.EncodeToString(payload)
	
	h := hmac.New(sha256.New, key)
	h.Write(payload)
	signature := h.Sum(nil)
	encSignature := base64.RawURLEncoding.EncodeToString(signature)
	
	return encPayload + "." + encSignature
}

func VerifyToken (token string) (nickname string, admin bool, err error) {
	if token == "" {
		return "", false, nil
	}
	l := strings.Split(token, ".")
	M.Assert(len(l) == 2, "len(l) =", len(l), 20)
	encPayload := l[0]
	encSignature := l[1]
	payload, err := base64.RawURLEncoding.DecodeString(encPayload); M.Assert(err == nil, err, 21)
	signature1, err := base64.RawURLEncoding.DecodeString(encSignature); M.Assert(err == nil, err, 22)
	h := hmac.New(sha256.New, key)
	h.Write(payload)
	signature2 := h.Sum(nil)
	b := hmac.Equal(signature1, signature2)
	var (j J.Json; o *J.Object; v J.Value; exp int64)
	if b {
		j = J.ReadString(string(payload))
		b = j != nil && j.JKind() == J.ObjectKind
	}
	if b {
		o = j.(*J.Object)
		v, b = J.GetValue(o, "nickname")
		if b {
			nickname, b = J.GetString(v)
		}
	}
	if b {
		v, b = J.GetValue(o, "admin")
		if b {
			admin, b = J.GetBool(v)
		}
	}
	if b {
		v, b = J.GetValue(o, "exp")
		if b {
			exp, b = J.GetInt(v)
		}
	}
	if !b {
		return "", false, errors.New("WrongToken")
	}
	if time.Now().Unix() > exp {
		return nickname, admin, errors.New("ExpiredToken")
	}
	return nickname, admin, nil
}

func init () {
	var err error
	tokenDuration, err = time.ParseDuration(tokenValidity); M.Assert(err == nil, err, 100)
	flag.StringVar(&manKey, "k", "", "Private Key")
	flag.Parse()
}
