package cryptoBasic

import (
	
	"testing"
	"fmt"

)

var (
	
	tok1, tok2, f1tok1, f1tok2, f2tok1, f2tok2 string

)

func TestGetKey (t *testing.T) {
	b := GetKey()
	fmt.Println("GetKey() =", b)
}

func TestCryptPass (t *testing.T) {
	pass := "gerard94"
	c1 := CryptPass(pass)
	fmt.Println("CryptPass =", c1)
	fmt.Println("VerifyPass", VerifyPass("gerard94", c1), VerifyPass("geronimo", c1))
}

func TestMakeToken (t *testing.T) {
	tok1 = MakeToken("gerard94", true)
	tok2 = MakeToken("geronimo", false)
	fmt.Println("Token1 =", tok1)
	fmt.Println("Token2 =", tok2)
	f1tok1 = MakeFalseToken1("gerard94", true, "geronimo")
	f1tok2 = MakeFalseToken1("geronimo", false, "gerard94")
	fmt.Println("f1Token1 =", f1tok1)
	fmt.Println("f1Token2 =", f1tok2)
	f2tok1 = MakeFalseToken2("gerard94", true)
	f2tok2 = MakeFalseToken2("geronimo", false)
	fmt.Println("f2Token1 =", f2tok1)
	fmt.Println("f2Token2 =", f2tok2)
}

func TestVerifyToken (t *testing.T) {
	n1, a1, err1 := VerifyToken(tok1)
	n2, a2, err2 := VerifyToken(tok2)
	fmt.Println("n1 =", n1, "a1 =", a1, "err1 =", err1)
	fmt.Println("n2 =", n2, "a2 =", a2, "err2 =", err2)
	n1, a1, err1 = VerifyToken(f1tok1)
	n2, a2, err2 = VerifyToken(f1tok2)
	fmt.Println("n1 =", n1, "a1 =", a1, "err1 =", err1)
	fmt.Println("n2 =", n2, "a2 =", a2, "err2 =", err2)
	n1, a1, err1 = VerifyToken(f2tok1)
	n2, a2, err2 = VerifyToken(f2tok2)
	fmt.Println("n1 =", n1, "a1 =", a1, "err1 =", err1)
	fmt.Println("n2 =", n2, "a2 =", a2, "err2 =", err2)
}
